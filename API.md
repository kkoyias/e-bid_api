# EndPoints

**Content-Type** defaults to: `application/json`  
**Rest.Controller** defaults to: `spring-boot-rest-controller`

**Base Path** `/api`

## Basic-Operations

### Clients

| Method     | Route                              | Action                                 | React.Component                   | Rest.Controller | Errors             |
|------------|------------------------------------|----------------------------------------|-----------------------------------|-----------------|--------------------|
| **GET**    | /clients                           | Read a client page from database       | Admin.Panel                       | ClientController|                    |
| **GET**    | /clients/{id}                      | Read a particular client from database | Admin.UserInfo/Guest.SignUp(edit) | -               |                    |
| **GET**    | /clients/{id}/bids                 | Get all items bid by a client          | User.Actions.Actions(bid mode)    | ClientController|                    |
| **GET**    | /clients/{id}/items                | Get all items offered by a client      | User.Actions.Actions(offer mode)  | -               |                    |
| **POST**   | /clients                           | Sign user up                           | Guest.SignUp                      | ClientController|  Username exists   |
| **PATCH**  | /clients/{id}                      | Edit user profile                      | Guest.SignUp(edit)                | ClientController|                    |
| **DELETE** | /clients/{id}                      | Delete user account(not offers though) | Guest.SignUp(edit)                | ClientController|                    |
| **GET**    | /clients/login                     | Log user in & locally  store profile   | Guest.SignIn                      | ClientController| Invalid credentials|
| **POST**   | /clients/{clientID}/views/{viewID} | Mark item as `viewed` by a client      | ProductView.ProductView(!owner)   | ClientController|  Client is owner   |
| **GET**    | /clients/{id}/recommendations      | Get recommended items for a client     | Homepage                          | `???`           |                    |

### Items

| Method     | Route                              | Action                        | React.Component                              | Rest.Controller | Errors                                                                                     |
|------------|------------------------------------|-------------------------------|----------------------------------------------|-----------------|--------------------------------------------------------------------------------------------|
| **GET**    | /items/{id}                        | Read a single item            | User.AddProduct.AddProduct(edit)/ProductPage | -               |                                                                                            |
| **POST**   | /items/{clientID}                  | Offer an item                 | User.AddProduct.AddProduct                   | ItemController  | Client not found / Invalid body format                                                     |
| **PATCH**  | /items/{id}                        | Edit a single item            | User.AddProduct.AddProduct(edit-mode)        | ItemController  | Auction has started or a bid has been made / Invalid body format                           |
| **DELETE** | /items/{id}                        | Remove an item(all bids also) | User.AddProduct.AddProduct(edit-mode)        | ItemController  | Auction has started or a bid has been made                                                 |
| **POST**   | /items/{itemID}/bids/{bidderID}    | Make a bid                    | ProductView.ProductView                      | ItemController  | Seller can not make bid on own item / Auction has started / A bid has been made / Item sold|
| **POST**   | /items/{itemID}/ratings/{clientID} | Rate an item                  | ProductView.ProductView                      | ItemController  | Seller can not rate own item / Auction has started / A bid has been made / Item sold       |

### Advanced

### Search

| Method     | Route                                                      | Action                                                                   | React.Component   | Rest.Controller     | Errors              |
|------------|------------------------------------------------------------|--------------------------------------------------------------------------|-------------------|---------------------|---------------------|
| **GET**    | /categories                                                | Get all main categories and their sub-categories(2 levels deep in total) | User.GetCategories| CategoryController  |-                    |
| **GET**    | /items/search?category=`..`&description=`..`&price=`..`    | Search by item category, description and maximum name                    | Search.Search     | ItemController      |Non existent category|

### Messenger

| Method   | Route                               | Action                                      | React.Component | Rest.Controller  | Errors                                     |
|----------|-------------------------------------|---------------------------------------------|-----------------|------------------|--------------------------------------------|
| **GET**  | /chats/{chatID}                     | Get the update chat only for a client       | Chat.Chat       | ChatController   | -                                          |
| **GET**  | /{clientID}/chats                   | Get all chats for a given client            | Chat.Chat       | ClientController | -                                          |
| **POST** | /{clientID}/chats/{sellerID}        | Once an auction is completed, create a chat | `???`           | ClientController | -                                          |
| **POST** | /chats/{chatID}/messages            | Send message                                | Chat.Chat       | ClientController | -                                          |

### Other

| Method     | Route                                         | Action                        | React.Component                  | Rest.Controller |
|------------|-----------------------------------------------|-------------------------------|----------------------------------|-----------------|
| **GET**    | /items                                        | Show Popular Items            | Welcome /Admin.Panel             | -               |
