drop procedure if exists findClients;

DELIMITER //
create procedure findClients(in clientID int(11), out rv int(11))
begin
    select rating_bidder into rv from client where id=clientID;
end //
 
DELIMITER ;

set @res=0;
call findClients(89, @res);
select @res;