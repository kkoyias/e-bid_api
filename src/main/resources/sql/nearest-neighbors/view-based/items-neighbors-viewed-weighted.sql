
# get all items neighbors viewed ordered by the # of neighbors that viewd them
(select item_id, count(similarity) 
 from client_viewed_item cvi inner join 
 
		# for each neighbor v of client u = 90, get id & R(u, v) (similarity function's value)
		(select c2.client_id, count(c2.client_id) as similarity
		 from client_viewed_item c1 inner join client_viewed_item c2
		 where c1.item_id = c2.item_id and c1.client_id != c2.client_id and c1.client_id = 89
		 group by c2.client_id
		 order by count(c2.client_id) desc
         limit 20
		)as neighbors on cvi.client_id  = neighbors.client_id
group by item_id
order by count(similarity) desc
);
