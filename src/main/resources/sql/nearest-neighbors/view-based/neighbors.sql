# get all users with the same views ordered by the # of same views(at most 50 of them)

select c2.client_id, count(c2.client_id)
from client_viewed_item c1 inner join client_viewed_item c2
where c1.item_id = c2.item_id and c1.client_id != c2.client_id and c1.client_id = 90
group by c2.client_id
order by count(c2.client_id) desc
limit 50;
