# get all items a client with same views has viewed

select *
from item i
where i.id in
	(select item_id 
     from client_viewed_item cvi inner join 
     
			# get neighbors table
			(select c2.client_id, count(c2.client_id) as similarity
			 from client_viewed_item c1 inner join client_viewed_item c2
			 where c1.item_id = c2.item_id and c1.client_id != c2.client_id and c1.client_id = 90
			 group by c2.client_id
			 order by count(c2.client_id) desc
			)as neighbors on cvi.client_id  = neighbors.client_id
	);
