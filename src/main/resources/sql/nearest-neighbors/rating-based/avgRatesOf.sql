drop function if exists avgRatesOf;

create function avgRatesOf(clientID int(11))
returns float(3) deterministic
return 
	(select avg(rating)
	from rating 
	where client_id = clientID);
    
select avgRatesOf(93);
#select * from rating where client_id=93;