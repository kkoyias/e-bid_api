select * 
from item 
where id in
	# get sum of neighbors latest ratings for each item 
	(select r.item_id
		from rating r inner join
        
        # get neighbors
		(select id, rating_bidder 
		 from client c2
		 where id=93 or id=94) 
         as neighbors 
	on r.client_id=neighbors.id 
    and not exists
		(select * from rating r1 
		 where r1.client_id = neighbors.id  and r1.item_id = r.item_id
			   and (r1.time > r.time or (r1.time = r.time and r1.id > r.id))) 
	
    # make sure this item is not sold or expired
    and r.item_id not in (select id from item where sold=1 or ends < "2019-10-10")  
	group by r.item_id
    order by sum(rating * neighbors.rating_bidder) desc)

	#as neighborRating on neighbors.item_id=i.id;