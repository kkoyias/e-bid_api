
(select r.client_id, 
	 sum(ratesOf89.rating * r.rating) 
     / (rootOfSumOfSquares(89) * rootOfSumOfSquares(r.client_id)) as similarity
from rating r inner join 

		# items 89 has rated & does not own with his latest rating on them
        (select r2.item_id, r2.rating 
		from rating r2
        where r2.client_id=89 and r2.item_id not in
				
				# not his own item
				(select id from item where seller_id=89)  
			and not exists
            
				# there is not a more recent rating of his on this item
				(select * 
                 from rating r3 
                 where r3.client_id=r2.client_id and r3.item_id=r2.item_id 
                 and (r3.time > r2.time or (r3.time = r2.time and r3.id > r2.id))) 
        ) as ratesOf89 on r.item_id=ratesOf89.item_id
        
# exclude our client from the neighbor list        
where r.client_id != 89 
and not exists 

	# there is not a more recent rating of his on this item
	(select * 
	 from rating r2
	 where r2.client_id=r.client_id and r2.item_id=r.item_id 
	 and (r2.time > r.time or (r2.time = r.time and r2.id > r.id))) 
group by r.client_id
order by sum(ratesOf89.rating * r.rating) desc
limit 50);