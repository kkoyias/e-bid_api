# uncomment next line & select r.item_id ONLY at line 8
# to get a full item list with all info for the recommended items

#select * from item #where id in

	# get sum of neighbors latest ratings, each of them multiplied
    # by the similarity degree, for each item of our database
	(select r.item_id, sum(rating * similarity) as r_value
		from rating r inner join
        
        # get neighbors
		(select r.client_id, 
			 sum(abs(ratesOf89.rating - avgRatesOf(93)) * abs(r.rating - avgRatesOf(r.client_id))) 
			 / (rootOfSumOfSquares(93-avgRatesOf(93)) * rootOfSumOfSquares(r.client_id -avgRatesOf(r.client_id))) 
             as similarity
		from rating r inner join 

				# items 89 has rated & does not own with his latest rating on them
				(select r2.item_id, r2.rating 
				from rating r2
				where r2.client_id=93 and r2.item_id not in
						
						# not his own item
						(select id from item where seller_id=93)  
					and not exists
					
						# there is not a more recent rating of his on this item
						(select * 
						 from rating r3 
						 where r3.client_id=r2.client_id and r3.item_id=r2.item_id 
						 and (r3.time > r2.time or (r3.time = r2.time and r3.id > r2.id))) 
				) as ratesOf89 on r.item_id=ratesOf89.item_id
				
		# exclude our client from the neighbor list        
		where r.client_id != 93
		and not exists 

			# there is not a more recent rating of his on this item
			(select * 
			 from rating r2
			 where r2.client_id=r.client_id and r2.item_id=r.item_id 
			 and (r2.time > r.time or (r2.time = r.time and r2.id > r.id))) 
		group by r.client_id
		order by sum(ratesOf89.rating * r.rating) desc
		limit 50)
         as neighbors 
	on r.client_id=neighbors.client_id
    and not exists
		(select * from rating r1 
		 where r1.client_id = neighbors.client_id  and r1.item_id = r.item_id
			   and (r1.time > r.time or (r1.time = r.time and r1.id > r.id))) 
	
    # make sure this item is not sold or expired
    and r.item_id not in (select id from item where sold=1 or ends < "2019-10-10")  
	group by r.item_id
    order by sum(rating * similarity) desc)
