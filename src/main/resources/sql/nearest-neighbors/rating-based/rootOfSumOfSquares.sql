drop function if exists rootOfSumOfSquares;
create function rootOfSumOfSquares(clientID int(11))
returns float(3) DETERMINISTIC
return (select sqrt(sum(rating*rating)) 
			from rating r2
			where r2.client_id=89 and r2.item_id not in
					
					# not his own item
					(select id from item where seller_id=89)  
				and not exists
				
					# there is not a more recent rating of his on this item
					(select * 
					 from rating r3 
					 where r3.client_id=r2.client_id and r3.item_id=r2.item_id 
					 and (r3.time > r2.time or (r3.time = r2.time and r3.id > r2.id)))); 
select rootOfSumOfSquares(89);