package com.ebid.api.rating;

import com.ebid.api.item.Item;
import com.ebid.api.user.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "rating")
@Data
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private int rating;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "item_id")
    @JsonIgnore
    private Item item;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private Client client;

    @Column(name = "time")
    private LocalDateTime timeStamp;

    @Override
    public String toString() {
        return "Rating{" +
                "id=" + id +
                ", rating=" + rating +
                ", item=" + item.getId() +
                ", client=" + client.getId() +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
