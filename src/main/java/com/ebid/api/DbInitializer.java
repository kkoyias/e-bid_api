package com.ebid.api;

import com.ebid.api.user.User;
import com.ebid.api.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "spring.jpa.hibernate.ddl-auto", havingValue = "create")
public class DbInitializer implements CommandLineRunner {
    private UserDAO userDAO;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public DbInitializer(UserDAO userDAO, PasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... strings) throws Exception {
        User admin = new User("admin", passwordEncoder.encode("admin"), "ROLE_ADMIN");
        this.userDAO.save(admin);
    }
}