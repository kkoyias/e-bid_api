package com.ebid.api.item;

import com.ebid.api.category.Category;
import com.ebid.api.location.Location;
import com.ebid.api.user.ClientDTO;
import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;
import java.util.List;

@Data
@XmlRootElement
public class ItemDTO {

    @XmlElement
    private int id;

    @XmlElement
    private String name;

    @XmlElement
    private Date startsAt;

    @XmlElement
    private Date endsAt;

    @XmlElement
    private String description;

    @XmlElement
    private float current;

    @XmlElement
    private float firstBid;

    @XmlElement
    private int numberOfBids;

    @XmlElement
    private float buyPrice;

    @XmlElement
    private boolean sold;

    @XmlElement
    private Location location;

    @XmlElement
    private List<Category> categories;

    @XmlElement
    private ClientDTO seller;

    @XmlElement
    private float bid;

    @XmlElement
    private float rating;

    @XmlElement
    private int userRating;

    ItemDTO(int id, String name, Date startsAt, Date endsAt, String description, float current,
            float firstBid, int numberOfBids, float buyPrice, boolean sold, Location location,
            List<Category> categories, ClientDTO seller, float rating) {
        this.id = id;
        this.name = name;
        this.startsAt = startsAt;
        this.endsAt = endsAt;
        this.description = description;
        this.current = current;
        this.firstBid = firstBid;
        this.numberOfBids = numberOfBids;
        this.buyPrice = buyPrice;
        this.sold = sold;
        this.location = location;
        this.categories = categories;
        this.seller = seller;
        this.rating = rating;
    }
}
