package com.ebid.api.item.dao;

import com.ebid.api.item.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource
public interface ItemDAO extends JpaRepository<Item, Integer>, ItemRepo {
}
