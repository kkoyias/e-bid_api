package com.ebid.api.item.dao;

import com.ebid.api.item.Item;
import com.ebid.api.item.ItemDTO;
import com.ebid.exception.ControllerException;

import java.util.List;

public interface ItemRepo {
    List<Item> searchItems(String description, Float price) throws ControllerException;
    List<Item> itemsByCategory(Integer categoryID) throws ControllerException;
    List<ItemDTO> listRecommendations(Integer clientID);

}
