package com.ebid.api.item.dao;

import com.ebid.api.item.Item;
import com.ebid.api.item.ItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Repository
public class ItemRepoImpl implements ItemRepo{

    private EntityManager entityManager;

    @Autowired
    public ItemRepoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // make a case-insensitive item search based on category or/and any text in any field
    public List<Item> searchItems(String description, Float price) {

        String descriptionCondition = description != null ?
                " and (i.description like '%" + description.toLowerCase() + "%' or i.name like '%"
                 + description.toLowerCase() + "%')" : "";

        String priceCondition = price != null ?
                " and first_bid < " + price + " and not exists (select * from bid where item_id = i.id and amount > " + price + ")" : "";

        String sqlQuery = "select * from item i where sold=0 and i.ends > '"
                + LocalDateTime.now() + "'" + descriptionCondition + priceCondition;

        Query q = this.entityManager.createNativeQuery(sqlQuery, Item.class);
        return q.getResultList();
    }

    // given a category, get a list of all items for this category
    @Override
    public List<Item> itemsByCategory(Integer categoryID) {

        // create HQL query
        String sqlQuery = "select * from item where sold=0 and ends > '" + LocalDateTime.now() + "'" +
                " and id in (select item_id from item_has_category where category_id=:category_id)";
        Query query = this.entityManager.createNativeQuery(sqlQuery, Item.class);
        query.setParameter("category_id", categoryID);

        return query.getResultList();
    }

    @Override
    public List<ItemDTO> listRecommendations(Integer clientID){
        final int maxNeighbors  = 20, minRecommendations = 10;

        // create query to return item list
        String sqlQuery =
                "select * from item where id in\n" +
                        "\n" +
                        "# get sum of neighbors latest ratings, each of them multiplied\n" +
                        "    # by the similarity degree, for each item of our database\n" +
                        "(select r.item_id\n" +
                        "from rating r inner join\n" +
                        "        \n" +
                        "        # get neighbors\n" +
                        "(select r.client_id, \n" +
                        " sum(ratesOfUser.rating * r.rating) \n" +
                        " / (rootOfSumOfSquares(" + clientID + " ) * rootOfSumOfSquares(r.client_id)) as similarity\n" +
                        "from rating r inner join \n" +
                        "\n" +
                        "# items " + clientID + "  has rated & does not own with his latest rating on them\n" +
                        "(select r2.item_id, r2.rating \n" +
                        "from rating r2\n" +
                        "where r2.client_id=" + clientID + "  and r2.item_id not in\n" +
                        "\n" +
                        "# not his own item\n" +
                        "(select id from item where seller_id=" + clientID + " )  \n" +
                        "and not exists\n" +
                        "\n" +
                        "# there is not a more recent rating of his on this item\n" +
                        "(select * \n" +
                        " from rating r3 \n" +
                        " where r3.client_id=r2.client_id and r3.item_id=r2.item_id \n" +
                        " and (r3.time > r2.time or (r3.time = r2.time and r3.id > r2.id))) \n" +
                        ") as ratesOfUser on r.item_id=ratesOfUser.item_id\n" +
                        "\n" +
                        "# exclude our client from the neighbor list        \n" +
                        "where r.client_id != " + clientID + "  \n" +
                        "and not exists \n" +
                        "\n" +
                        "# there is not a more recent rating of his on this item\n" +
                        "(select * \n" +
                        " from rating r2\n" +
                        " where r2.client_id=r.client_id and r2.item_id=r.item_id \n" +
                        " and (r2.time > r.time or (r2.time = r.time and r2.id > r.id))) \n" +
                        "group by r.client_id\n" +
                        "order by sum(ratesOfUser.rating * r.rating) desc\n" +
                        "limit " + maxNeighbors  + ")\n" +
                        "         as neighbors \n" +
                        "on r.client_id=neighbors.client_id\n" +
                        "    and not exists\n" +
                        "(select * from rating r1 \n" +
                        " where r1.client_id = neighbors.client_id  and r1.item_id = r.item_id\n" +
                        "   and (r1.time > r.time or (r1.time = r.time and r1.id > r.id))) \n" +
                        "\n" +
                        "    # make sure this item is not sold or expired\n" +
                        "    and r.item_id not in (select id from item where sold=1)  \n" +
                        "group by r.item_id\n" +
                        "    order by sum(rating * similarity) desc)";

        Query query = this.entityManager.createNativeQuery(sqlQuery, Item.class);

        List<Item> recommendations = query.getResultList();

        // if not enough rating-based recommendations where found, display view-based recommendations as well
        if(recommendations.size() < minRecommendations){
            sqlQuery =
                    "select * from item where seller_id != " + clientID + " and sold=0 and id in\n" +
                            "(select item_id\n" +
                            " from client_viewed_item cvi inner join \n" +
                            " \n" +
                            "\t\t# for each neighbor v of client u = 90, get id & R(u, v) (similarity function value)\n" +
                            "\t\t(select c2.client_id, count(c2.client_id) as similarity\n" +
                            "\t\t from client_viewed_item c1 inner join client_viewed_item c2\n" +
                            "\t\t where c1.item_id = c2.item_id and c1.client_id != c2.client_id and c1.client_id = " + clientID +
                            "\n" +
                            "\t\t group by c2.client_id\n" +
                            "\t\t order by count(c2.client_id) desc\n" +
                            "         limit " + maxNeighbors + "\n" +
                            "\t\t)as neighbors on cvi.client_id  = neighbors.client_id\n" +
                            "group by item_id\n" +
                            "order by count(similarity) desc\n" +
                            ");";
            query = this.entityManager.createNativeQuery(sqlQuery, Item.class);
            recommendations.addAll(query.getResultList());
        }

        // execute and transform result to itemDTO list
        List<ItemDTO> result = new ArrayList<>();
        for(Object item : new ArrayList<>(new HashSet<>(recommendations)))
            result.add(Item.toDTO((Item)item));

        return result;
    }
}
