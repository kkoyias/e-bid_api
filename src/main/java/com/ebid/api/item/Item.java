package com.ebid.api.item;

import com.ebid.api.bid.Bid;
import com.ebid.api.category.Category;
import com.ebid.api.location.Location;
import com.ebid.api.rating.Rating;
import com.ebid.api.user.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item")
@Data
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private String name;

    @Column(name = "starts")
    private Date startsAt;

    @Column(name = "ends")
    private Date endsAt;

    @Column
    private String description;

    @Column(name = "first_bid")
    private float firstBid;

    @Column(name = "buy_price")
    private float buyPrice;

    @Column(name = "sold")
    private boolean sold;

    // the deletion of an item shall not remove it's location, but the creation shall create it
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST})
    @JoinColumn(name = "location_id")
    private Location location;

    // a seller can offer a lot of items, the deletion of which shall not affect that client
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "seller_id", columnDefinition = "integer")
    @JsonIgnore
    private Client seller;

    // the deletion of which shall not affect the client that bought it
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "buyer_id", columnDefinition = "integer")
    @JsonIgnore
    private Client buyer;


    // there might be plenty of bids for an item, the deletion of whom shall remove all the corresponding bids
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "item")
    private List<Bid> bids;

    // there might be plenty of ratings for an item, the deletion of whom shall remove all the corresponding ratings
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "item")
    private List<Rating> ratings;

    // an item can be of many categories, no category should be created if not exists or erased if exists
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "item_has_category",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private List<Category> categories;

    // get the bid list length
    public int numberOfBids(){
        return this.bids.size();
    };

    // get the maximum bid until now or, if there is none, the firstBid
    public float current(){
        return this.bids == null || this.bids.isEmpty() ? this.firstBid : Bid.getMax(this.bids).getAmount();
    }

    public float rating(){
        return ratings == null || ratings.isEmpty() ? 0 :
                (float)ratings.stream().mapToInt(Rating::getRating).sum()/ratings.size();
    }

    // add a given category to the category list if it does not exist
    void addCategory(Category category){
        if(this.categories == null)
            this.categories = new ArrayList<>();
        if(!this.categories.contains(category))
            this.categories.add(category);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startsAt=" + startsAt +
                ", endsAt=" + endsAt +
                ", description='" + description + '\'' +
                ", firstBid=" + firstBid +
                ", buyPrice=" + buyPrice +
                ", sold=" + sold +
                ", seller={ " + (seller != null ? "id: " + seller.getId() + ", username: " + seller.getUserName() : "") + '}' +
                ", buyer={ " + (buyer != null ? "id: " + buyer.getId() + ", username: " + buyer.getUserName() : "") + '}' +
                ", bids=" + bids +
                ", categories=" + categories +
                '}';
    }

    // determine whether an offered item can be updated or deleted
    @JsonIgnore
    public boolean isAlterable(){
        Date present = new Date(System.currentTimeMillis());
        return (this.getBids().size() == 0 && this.startsAt.compareTo(present) > 0);
    }

    public static ItemDTO toDTO(Item item){
        ItemDTO itemDTO = new ItemDTO(item.getId(), item.getName(), item.getStartsAt(), item.getEndsAt(),
                item.getDescription(), item.current(), item.getFirstBid(), item.numberOfBids(),
                item.getBuyPrice(), item.isSold(), item.getLocation(),item.getCategories(),
                Client.toDTO(item.getSeller()), item.rating());
        return itemDTO;
    }
}
