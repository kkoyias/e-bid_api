package com.ebid.api.item;

import com.ebid.Utils;
import com.ebid.api.bid.Bid;
import com.ebid.api.bid.BidDAO;
import com.ebid.api.category.Category;
import com.ebid.api.category.CategoryDAO;
import com.ebid.api.item.dao.ItemDAO;
import com.ebid.api.rating.Rating;
import com.ebid.api.rating.RatingDAO;
import com.ebid.api.user.Client;
import com.ebid.api.user.ClientDAO;
import com.ebid.exception.ControllerException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@RepositoryRestController
@RequestMapping("/items")
public class ItemController {

    // inject item, bid and client repositories
    private ItemDAO itemDAO;
    private ClientDAO clientDAO;
    private BidDAO bidDAO;
    private CategoryDAO categoryDAO;
    private RatingDAO ratingDAO;
    private ObjectMapper  objectMapper;
    private EntityManager entityManager;

    @Autowired
    public ItemController(ItemDAO itemDAO, ClientDAO clientDAO, BidDAO bidDAO, CategoryDAO categoryDAO,
                          RatingDAO ratingDAO, ObjectMapper objectMapper, EntityManager entityManager){
        this.itemDAO = itemDAO;
        this.clientDAO = clientDAO;
        this.bidDAO = bidDAO;
        this.categoryDAO = categoryDAO;
        this.ratingDAO = ratingDAO;
        this.objectMapper = objectMapper;
        this.entityManager = entityManager;
    }

    @GetMapping
    public @ResponseBody ResponseEntity<?> getItems() throws IOException {

        // get all offered items
        List<Item> items = this.itemDAO.findAll();

        // sort them by number of bids, from the most to the least popular
        items.sort(Collections.reverseOrder(Comparator.comparing(Item::numberOfBids)));

        // transform each of them to an ItemDTO and return the result list
        return new ResponseEntity<>(items.stream().map(Item::toDTO), HttpStatus.OK);
    }

    @GetMapping("/{itemID}")
    public @ResponseBody ResponseEntity<?> getItem(@RequestHeader(value = "Identification", required = false) Integer id,
                                                   @PathVariable Integer itemID) throws ControllerException{

        Item item = (Item) Utils.findById(this.itemDAO, itemID);
        ItemDTO itemDTO = Item.toDTO(item);
        Client client = id != null ? this.clientDAO.findById(id).orElse(null) : null;

        // if this was a client request
        if(client != null){

            // include the amount of the last bid he made on that item, if any
            String sqlQuery = "select * from bid where bidder_id=:bidder and item_id=:item order by time desc";
            Query query = this.entityManager.createNativeQuery(sqlQuery, Bid.class);
            query.setParameter("bidder", client.getId()).setParameter("item", item.getId());

            List<Bid> results = query.getResultList();
            if(!results.isEmpty())
                itemDTO.setBid(results.get(0).getAmount());

            // also include the rating he's made on that item if any
            sqlQuery = "select * from rating where client_id=:client and item_id=:item order by time desc";
            query = this.entityManager.createNativeQuery(sqlQuery, Rating.class);
            query.setParameter("client", client.getId()).setParameter("item", item.getId());

            List<Rating> ratings = query.getResultList();
            if(!ratings.isEmpty())
                itemDTO.setUserRating(ratings.get(0).getRating());
        }

        return new ResponseEntity<>(itemDTO, HttpStatus.OK);
    }

    @GetMapping("/search")
    public @ResponseBody ResponseEntity<?> getItemsByCategory(
            @RequestParam(required = false) Integer categoryID,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) Float price) throws ControllerException{

        // combine results for category and description, price search, removing duplicates
        List<Item> result = new ArrayList<>();
        if(categoryID != null)
            result.addAll(this.itemDAO.itemsByCategory(categoryID));
        if(description != null || price != null) {
            result.addAll(this.itemDAO.searchItems(description, price));
            result = new ArrayList<>(new HashSet<>(result));
        }

        // keep just the fields necessary returning a list of itemDTOs
        List<ItemDTO> rv = result.stream().map(Item::toDTO).collect(Collectors.toList());

        return new ResponseEntity<>(rv, HttpStatus.OK);
    }

    @PostMapping("/{sellerID}")
    public @ResponseBody ResponseEntity<Item> saveItem(@PathVariable Integer sellerID,
                                                       @RequestBody Map<String, Object> payload) throws ControllerException, IllegalAccessException {

        return new ResponseEntity<>(this.saveOrUpdateItem(sellerID, payload, false), HttpStatus.CREATED);
    }

    @PatchMapping("/{itemID}")
    public @ResponseBody ResponseEntity<?> updateItem(@PathVariable Integer itemID,
                                                         @RequestBody Map<String, Object> payload) throws ControllerException, IllegalAccessException {
        return new ResponseEntity<>(this.saveOrUpdateItem(itemID, payload, true), HttpStatus.OK);
    }

    private Item saveOrUpdateItem(int id, Map<String, Object> payload, boolean update) throws ControllerException, IllegalAccessException{
        if(!payload.containsKey("item") || !payload.containsKey("categories"))
            throw new ControllerException("Appropriate format is {item: ...,categories: [...]", HttpStatus.BAD_REQUEST);

        // create an item holding the updated fields
        Item newItem = this.objectMapper.convertValue(payload.get("item"), Item.class);

        // get all categories from the request and set the item's categories appropriately
        List<Integer> categories = this.objectMapper.convertValue(payload.get("categories"), List.class);
        for(Integer categoryID : categories)
            newItem.addCategory((Category) Utils.findById(this.categoryDAO, categoryID));
        System.out.println(newItem);

        // if an id was passed, this is an update
        if(update) {

            // get the current version of the item
            Item item = (Item) Utils.findById(this.itemDAO, id);

            // check whether item update is allowed
            if (!item.isAlterable())
                throw new ControllerException("Item can not be updated: not alterable", HttpStatus.METHOD_NOT_ALLOWED);

            // update the object
            newItem = (Item) Utils.updateObject(item, newItem);
        }

        // else if this is a new item
        else{

            // set item seller appropriately
            Client seller = (Client) Utils.findById(this.clientDAO, id);
            newItem.setSeller(seller);
            newItem.setSold(false);
        }

        // store and return the brand new or updated version
        this.itemDAO.save(newItem);
        return newItem;
    }

    @DeleteMapping("/{itemID}")
    public @ResponseBody ResponseEntity<?> deleteItem(@PathVariable Integer itemID) throws ControllerException{
        Item item = (Item) Utils.findById(this.itemDAO, itemID);

        if(!item.isAlterable())
            throw new ControllerException("Item can not be removed: not alterable", HttpStatus.METHOD_NOT_ALLOWED);
        this.itemDAO.delete(item);
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @PostMapping("/{itemID}/bids/{bidderID}")
    public @ResponseBody ResponseEntity<?> makeBid(@PathVariable Integer itemID,
                                                    @PathVariable int bidderID,
                                                    @RequestBody Bid bid) throws ControllerException {
        System.out.println("Bidder: " + bidderID + " ItemID: " + itemID);

        int ratingPoints = 5;

        // ensure that the bidder and the item both exist in our database
        Client bidder = (Client) Utils.findById(clientDAO, bidderID);
        Item item = (Item) Utils.findById(itemDAO, itemID);

        // forbid bidding your own item or an expired one
        if (bidderID == item.getSeller().getId() || item.isSold())
            throw new ControllerException("Bidding own item is not allowed", HttpStatus.METHOD_NOT_ALLOWED);

        // update bid info
        bid.setBidder(bidder);
        bid.setItem(item);
        if(bid.getTimeStamp() == null)
            bid.setTimeStamp(LocalDateTime.now());

        // store bid in data base
        this.bidDAO.save(bid);

        // update item status if it was actually bought
        if (bid.getAmount() >= item.getBuyPrice()) {
            item.setSold(true);
            item.setBuyer(bidder);

            // increase owner's buyer rating
            item.getSeller().setRatingSeller(item.getSeller().getRatingSeller() + ratingPoints);

            // increase buyer's buyer rating and add item to his purchase list
            bidder.setRatingBidder(bidder.getRatingBidder() + ratingPoints);
            bidder.addPurchase(item);
            this.itemDAO.save(item);
        }

        return new ResponseEntity<>(bid, HttpStatus.CREATED);
    }

    @PostMapping("/{itemID}/ratings/{clientID}")
    public @ResponseBody ResponseEntity<?> rateItem(@PathVariable Integer itemID,
                                                    @PathVariable int clientID,
                                                    @RequestBody Rating rating) throws ControllerException{
        // ensure that both the client and the item both exist in our database
        Client client = (Client) Utils.findById(clientDAO, clientID);
        Item item = (Item) Utils.findById(itemDAO, itemID);

        // forbid rating your own item or a sold one
        if(clientID == item.getSeller().getId() || item.isSold())
            throw new ControllerException("Rating own item is not allowed", HttpStatus.METHOD_NOT_ALLOWED);

        // update rating info
        rating.setClient(client);
        rating.setItem(item);
        if(rating.getTimeStamp() == null)
            rating.setTimeStamp(LocalDateTime.now());

        // store rating in data base
        this.ratingDAO.save(rating);

        return new ResponseEntity<>(rating, HttpStatus.CREATED);
    }
}
