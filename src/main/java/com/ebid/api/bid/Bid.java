package com.ebid.api.bid;

import com.ebid.api.item.Item;
import com.ebid.api.user.Client;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "bid")
@Data
public class Bid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private float amount;

    @Column(name = "time")
    private LocalDateTime timeStamp;

    // a Client can bid many times, the deletion of a bit shall not affect the client
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "bidder_id")
    @JsonIgnore
    private Client bidder;

    // multiple bids can refer to a particular item and the deletion of those shall not affect the item
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "item_id")
    @JsonIgnore
    private Item item;

    // extract the maximum amount Bid from a bid list
    public static Bid getMax(List<Bid> bids){
        if(bids == null || bids.isEmpty())
            return null;
        Bid maxBid = bids.get(0);
        for(Bid bid : bids){
            if(bid.getAmount() > maxBid.getAmount())
                maxBid = bid;
        }
        return maxBid;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", bidder=" + bidder.getId() +
                ", item= {id : " + item.getId() + ", name: " + item.getName() +
                "}}";
    }
}
