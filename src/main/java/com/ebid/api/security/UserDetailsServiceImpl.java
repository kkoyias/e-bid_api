package com.ebid.api.security;

import com.ebid.api.user.User;
import com.ebid.api.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	UserDAO userDAO;

	@Autowired
	public UserDetailsServiceImpl(UserDAO userDAO){
		this.userDAO = userDAO;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String userName) {
		User user = this.userDAO.findByUserName(userName);

		return UserPrinciple.build(user);
	}
}
