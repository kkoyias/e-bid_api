package com.ebid.api.security;
/*
import com.ebid.api.user.Client;
import com.ebid.api.user.ClientDAO;
import com.ebid.api.user.User;
import com.ebid.api.user.UserDAO;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.StringTokenizer;

import static java.util.Base64.getDecoder;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    private AuthenticationManager authenticationManager;
    private ClientDAO clientDAO;
    private UserDAO userDAO;
    private JwtProvider jwtProvider;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, ClientDAO clientDAO,
                                    UserDAO userDAO, JwtProvider jwtProvider){
        this.authenticationManager = authenticationManager;
        this.clientDAO = clientDAO;
        this.userDAO = userDAO;
        this.jwtProvider = jwtProvider;
    }

    @GetMapping
    public ResponseEntity<String> authenticateUser(@RequestHeader("Authorization") String header) throws JSONException {

        // extract username and password from the header
        StringTokenizer stk = new StringTokenizer(new String(getDecoder().decode(header)), ":");
        String userName = stk.nextToken();
        String password = stk.nextToken();

        System.out.println(userName + " : " + password);

        // compare the password submitted against the one loaded by the UserDetailsService
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userName, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);

        User user = this.userDAO.findByUserName(userName);
        Optional<Client> client = this.clientDAO.findById(user.getId());

        String role = client.isPresent() ? "ROLE_CLIENT" : "ROLE_ADMIN";
        if (role.equals("ROLE_CLIENT") && !client.get().getStatus().equals("active"))
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        JSONObject response = new JSONObject();

        response.put("accessToken", jwt);
        response.put("userRole", role);
        response.put("userId", user.getId());

        return ResponseEntity.ok(response.toString());
    }
}*/
