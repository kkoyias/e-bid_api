package com.ebid.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthEntryPoint implements AuthenticationEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthEntryPoint.class);

	// Overrides AuthenticationEntryPoint.commence which is triggered
	// when a User requests a secure HTTP resource without being authenticated
	// because an AuthenticationException is thrown
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
			throws IOException{

		logger.error("Unauthorized error. Message - {}", e.getMessage()); // print the exception to the log
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error -> Unauthorized"); // return a Unauthorized
																							// response error
	}
}
