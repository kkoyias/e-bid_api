package com.ebid.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtProvider tokenProvider;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthTokenFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			System.out.println(request);
			String jwt = getJwt(request); // get only the JWT token
			if (jwt != null && tokenProvider.validateJwtToken(jwt)) { // validates JWT token using JwtProvider
				String username = tokenProvider.getUserNameFromJwtToken(jwt); // extract user information

				UserDetails userDetails = userDetailsService.loadUserByUsername(username); // extract user information
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken( // create
																												// authentication
																												// token
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request)); // set the
																										// details
																										// inside
																										// authentication
																										// object

				SecurityContextHolder.getContext().setAuthentication(authentication); // Store Authentication object in
																						// SecurityContext
			}
		} catch (Exception e) {
			logger.error("Can NOT set user authentication -> Message: {}", e);
		}

		filterChain.doFilter(request, response); // return the response
	}

	public String getJwt(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization"); // get the token with the bearer

		if (authHeader != null && authHeader.startsWith("Bearer ")) { // be sure it is in correct form
			return authHeader.replace("Bearer ", ""); // remove the "bearer " in front of the token
		}

		return null;
	}

	public String getJwt(String authHeader) {

		if (authHeader != null && authHeader.startsWith("Bearer ")) { // be sure it is in correct form
			return authHeader.replace("Bearer ", ""); // remove the "bearer " in front of the token
		}

		return null;
	}
}
