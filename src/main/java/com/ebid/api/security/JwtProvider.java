package com.ebid.api.security;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

	private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);

	@Value("${security.jwtSecret}") // get a secret key which is provided inside application properties
	private String jwtSecret;

	@Value("${security.jwtExpiration}") // get how much time for the token to expire from application properties
	private int jwtExpiration;

	public String generateJwtToken(Authentication authentication) {

		UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal(); // get user details and authorities
																						// from authentication

		return Jwts.builder() // build and return a jwt
				.setSubject((userPrincipal.getUsername())) // with the username
				.setIssuedAt(new Date()) // with the date that was created
				.setExpiration(new Date((new Date()).getTime() + jwtExpiration * 1000)) // with the time that it will
																						// expire
				.signWith(SignatureAlgorithm.HS512, jwtSecret) // and create signature with a specific algorithm using
																// the jwtSecret
				.compact();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken); // check the validation of the
																				// authorization token
			return true;
		} catch (SignatureException e) { // if something is wrong throw the appropriate exception
			logger.error("Invalid JWT signature -> Message: {} ", e);
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token -> Message: {}", e);
		} catch (ExpiredJwtException e) {
			logger.error("Expired JWT token -> Message: {}", e);
		} catch (UnsupportedJwtException e) {
			logger.error("Unsupported JWT token -> Message: {}", e);
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty -> Message: {}", e);
		}

		return false;
	}

	public String getUserNameFromJwtToken(String token) { // parse through the valid tokens in order to get the username
															// connected to a valid token
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}
}
