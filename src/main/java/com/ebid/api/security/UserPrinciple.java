package com.ebid.api.security;

import com.ebid.api.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

@Data
public class UserPrinciple implements UserDetails {
	private static final long serialVersionUID = 1L;

	private int id;

	private String userName;

	@JsonIgnore // in order not to return the password to the user along with the other info
	private String password;

	private UserPrinciple(int id, String userName, String password) {
		this.id = id;
		this.userName = userName;
		this.password = password;
	}

	public static UserPrinciple build(User user) { // return the user details in a UserPrinciple object

		return new UserPrinciple(user.getId(), user.getUserName(), user.getPassword());
	}

	public int getId() {
		return id;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserPrinciple user = (UserPrinciple) o;
		return Objects.equals(id, user.id);
	}
}
