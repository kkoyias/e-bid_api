package com.ebid.api.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import java.util.List;

@RepositoryRestController
@RequestMapping("/categories")
public class CategoryController {

    private EntityManager entityManager;

    @Autowired
    public CategoryController(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @GetMapping
    public @ResponseBody ResponseEntity<?> getMainCategories(){
        String hqlQuery = "from Category category where category.parentCategory=null";
        List<Category> rv = this.entityManager.createQuery(hqlQuery).getResultList();
        return new ResponseEntity<>(rv, HttpStatus.OK);
    }
}
