package com.ebid.api.chat;

import com.ebid.api.user.ClientDTO;
import lombok.Data;

import java.util.Date;

@Data
public class MessageDTO {

    private int id;
    private String content;
    private Date timeStamp;
    private ClientDTO sender;

    public MessageDTO(int id, String content, Date timeStamp, ClientDTO sender) {
        this.id = id;
        this.content = content;
        this.timeStamp = timeStamp;
        this.sender = sender;
    }
}
