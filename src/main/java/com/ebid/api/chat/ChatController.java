package com.ebid.api.chat;

import com.ebid.Utils;
import com.ebid.exception.ControllerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RepositoryRestController
@RequestMapping("/chats")
public class ChatController {
    private ChatDAO chatDAO;

    @Autowired
    public ChatController(ChatDAO chatDAO){
        this.chatDAO = chatDAO;
    }

    @GetMapping("/{chatID}")
    public @ResponseBody
    ResponseEntity<?> getChats(@PathVariable Integer chatID) throws ControllerException {
        Chat chat = (Chat) Utils.findById(this.chatDAO, chatID);
        ChatDTO chatDTO = Chat.toDTO(chat);

        return new ResponseEntity<>(chatDTO, HttpStatus.OK);
    }
}
