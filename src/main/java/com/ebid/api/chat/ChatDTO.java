package com.ebid.api.chat;

import com.ebid.api.user.ClientDTO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ChatDTO {

    private int id;
    private List<MessageDTO> messages;
    private List<ClientDTO> participants;
    private Date lastModified;

    public ChatDTO(int id, List<MessageDTO> messages, List<ClientDTO> participants, Date lastModified) {
        this.id = id;
        this.messages = messages;
        this.participants = participants;
        this.lastModified = lastModified;
    }
}
