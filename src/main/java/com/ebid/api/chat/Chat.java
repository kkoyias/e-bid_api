package com.ebid.api.chat;

import com.ebid.api.user.Client;
import com.ebid.api.user.ClientDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name = "chat")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int id;

    @Column
    private Date lastModified;

    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "chat")
    private List<Message> messages;

    // a client can have many chats the deletion of which shall not delete any client account
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "client_has_chat",
            joinColumns = @JoinColumn(name = "chat_id"),
            inverseJoinColumns = @JoinColumn(name = "client_id")
    )
    @JsonIgnore
    private List<Client> clients;

    public void addMessage(Message message){
        this.messages.add(message);
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", messages=" + messages +
                '}';
    }

    public static ChatDTO toDTO(Chat chat){

        // get message list data transfer objects sorted
        List<MessageDTO> messages = new ArrayList<>();
        for(Message msg : chat.getMessages())
            messages.add(Message.toDTO(msg));
        messages.sort(Comparator.comparing(MessageDTO::getTimeStamp));

        // get client/participant list data transfer objects
        List<ClientDTO> participants = new ArrayList<>();
        for(Client client : chat.getClients())
            participants.add(Client.toDTO(client));
        return new ChatDTO(chat.getId(), messages, participants, chat.getLastModified());
    }
}
