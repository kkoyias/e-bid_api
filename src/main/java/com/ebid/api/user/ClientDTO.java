package com.ebid.api.user;

import lombok.Data;

@Data
public class ClientDTO {
    private int id;
    private String userName;
    private int ratingSeller;

    public ClientDTO(int id, String userName, int ratingSeller) {
        this.id = id;
        this.userName = userName;
        this.ratingSeller = ratingSeller;
    }
}
