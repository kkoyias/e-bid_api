package com.ebid.api.user;

import com.ebid.api.bid.Bid;
import com.ebid.api.chat.Chat;
import com.ebid.api.item.Item;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "client")
@Data
public class Client extends User {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    private String email;

    @Column
    private String phone;

    @Column
    private int vat;

    @Column
    private String status;

    @Column(name = "rating_seller")
    private int ratingSeller;

    @Column(name = "rating_bidder")
    private int ratingBidder;

    @Column
    private String address;

    // a client can offer many items, deleting the account of the client shall remove all of his current offered items
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "seller")
    private List<Item> items;

    // a client can buy many items, deleting the account of the client shall not remove any of the items bought
    // the history section of the seller uses them & the administrator must be able to export ALL of the items
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "buyer")
    private List<Item> purchases;

    // a client can make many bids, deleting the account of the client shall remove all of his current bids
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bidder")
    @JsonIgnore
    private List<Bid>  bids;

    // all viewed items should be stored in order to be able to recommend new items to a user
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "client_viewed_item",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id")
    )
    private List<Item> views;

    // a client can have many chats none of which shall be deleted in case of an account deletion
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "client_has_chat",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "chat_id")
    )
    @JsonIgnore
    private List<Chat> chats;

    public void addView(Item view){
        this.views.add(view);
    }
    public void addPurchase(Item purchase){this.purchases.add(purchase);}

    public static ClientDTO toDTO(Client client){
        return new ClientDTO(client.getId(), client.getUserName(), client.getRatingSeller());
    }
}
