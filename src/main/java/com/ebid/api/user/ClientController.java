package com.ebid.api.user;

import com.ebid.Utils;
import com.ebid.api.chat.Chat;
import com.ebid.api.chat.ChatDAO;
import com.ebid.api.chat.ChatDTO;
import com.ebid.api.chat.Message;
import com.ebid.api.item.Item;
import com.ebid.api.item.ItemDTO;
import com.ebid.api.item.dao.ItemDAO;
import com.ebid.api.security.JwtProvider;
import com.ebid.exception.ControllerException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;

import static java.util.Base64.getDecoder;

@RepositoryRestController
@RequestMapping("/clients")
public class ClientController {

    private UserDAO userDAO;
    private ClientDAO clientDAO;
    private ItemDAO itemDAO;
    private ChatDAO chatDAO;
    private EntityManager entityManager;
    private PasswordEncoder passwordEncoder;
    private AuthenticationManager authenticationManager;
    private JwtProvider jwtProvider;

    @Autowired
    public ClientController(UserDAO userDAO, ClientDAO clientDAO, ItemDAO itemDAO, ChatDAO chatDAO,
                            EntityManager entityManager, PasswordEncoder passwordEncoder,
                            AuthenticationManager authenticationManager, JwtProvider jwtProvider) {
        this.userDAO = userDAO;
        this.clientDAO = clientDAO;
        this.itemDAO = itemDAO;
        this.chatDAO = chatDAO;
        this.entityManager = entityManager;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @GetMapping
    public @ResponseBody ResponseEntity<?> getClients(){
        return new ResponseEntity<>(this.clientDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping("/login")
    public @ResponseBody ResponseEntity<String> login(@RequestHeader("Authorization") String header) throws JSONException {

        // extract username and password from the header
        StringTokenizer stk = new StringTokenizer(new String(getDecoder().decode(header)), ":");
        if(stk.countTokens() != 2)
            return new ResponseEntity<>("Invalid header", HttpStatus.BAD_REQUEST);
        String userName = stk.nextToken();
        String password = stk.nextToken();

        System.out.println(userName + " : " + password);
        User user = this.userDAO.findByUserName(userName);

        // compare the password submitted against the one loaded by the UserDetailsService
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);

        Optional<Client> client = this.clientDAO.findById(user.getId());

        String role = user.getRole();
        if (role.equals("ROLE_CLIENT") && !client.get().getStatus().equals("active"))
            return new ResponseEntity<>(HttpStatus.UPGRADE_REQUIRED);

        JSONObject response = new JSONObject();

        response.put("accessToken", jwt);
        response.put("userRole", role);
        response.put("userId", user.getId());
        response.put("userName", user.getPassword());

        return ResponseEntity.ok(response.toString());
    }

    @GetMapping("/{clientID}/bids")
    public @ResponseBody ResponseEntity<?> getBids(@PathVariable Integer clientID){

        // create query to return item list
        String sqlQuery = "select * from item where id in (select item_id from bid where bidder_id=:id)";
        Query query = this.entityManager.createNativeQuery(sqlQuery, Item.class);
        query.setParameter("id", clientID);

        // execute and transform result to itemDTO list
        List<ItemDTO> result = new ArrayList<>();
        for(Object item : query.getResultList())
            result.add(Item.toDTO((Item)item));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{clientID}/recommendations")
    public @ResponseBody ResponseEntity<?> getRecommendations(@PathVariable Integer clientID){
        return new ResponseEntity<>(this.itemDAO.listRecommendations(clientID), HttpStatus.OK);
    }

    @PatchMapping("/{clientID}")
    public @ResponseBody ResponseEntity<Client> editProfile(@PathVariable Integer clientID,
                                                            @RequestHeader("Authorization") String password,
                                                            @RequestBody Client client) throws ControllerException, IllegalAccessException{
        Client oldClient = (Client)Utils.findById(this.clientDAO, clientID);
        Utils.updateObject(oldClient, client);

        password = new String(getDecoder().decode(password));
        if(!password.equals("admin"))
            oldClient.setPassword(passwordEncoder.encode(new String(getDecoder().decode(password))));
        this.clientDAO.save(oldClient);
        return new ResponseEntity<>(oldClient, HttpStatus.OK);
    }

    // sing-user up
    @PostMapping
    public @ResponseBody ResponseEntity<?> join(@RequestHeader("Authorization") String password,
                                                @RequestBody Client client){

        // ensure that this user name does not exist already
        if(this.userDAO.findByUserName(client.getUserName()) != null)
            return new ResponseEntity<>(HttpStatus.CONFLICT);

        System.out.println("sign-up: " + password + " : " + (new String(getDecoder().decode(password))));

        // decode header to get the password, then encode it and store client's info in our database
        client.setPassword(passwordEncoder.encode(new String(getDecoder().decode(password))));
        client.setRole("ROLE_CLIENT");
        this.clientDAO.save(client);

        return new ResponseEntity<>(client, HttpStatus.CREATED);
    }

    /*@DeleteMapping("/{clientID}")
    public @ResponseBody ResponseEntity<?> deleteClient(@PathVariable Integer clientID) throws ControllerException{
        Client client = (Client)Utils.findById(this.clientDAO, clientID);

        // do not allow clients to delete their accounts while auctions are active, suggest cancelling those
        if(!client.getItems().isEmpty())
            return new ResponseEntity<>("Unable to delete: cancel all auctions of this client first, please", HttpStatus.METHOD_NOT_ALLOWED);

        this.clientDAO.delete(client);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }*/

    // mark items as viewed when visited by a client
    @PostMapping("/{clientID}/views/{itemID}")
    public @ResponseBody ResponseEntity<?> addView(@PathVariable Integer clientID,
                                                   @PathVariable Integer itemID) throws ControllerException {

        // get client and viewed item
        Client client = (Client) Utils.findById(this.clientDAO, clientID);
        Item item = (Item) Utils.findById(this.itemDAO, itemID);

        // if the client has already viewed this item, return 208
        if(client.getViews().contains(item))
            return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);

        // if client owns the item, ignore
        if(client.getItems().contains(item))
            throw new ControllerException("Client owns the item: View ignored", HttpStatus.METHOD_NOT_ALLOWED);

        // update view list adding the corresponding item
        client.addView(item);
        this.clientDAO.save(client);

        return new ResponseEntity<Client>(client, HttpStatus.OK);
    }

    @GetMapping("/{clientID}/chats")
    public @ResponseBody ResponseEntity<?> getChats(@PathVariable Integer clientID) throws ControllerException{

        // get the client
        Client client = (Client) Utils.findById(this.clientDAO, clientID);

        // get all of his chats, turn them into ChatDTOs and sort them using the last modification time
        List<ChatDTO> chats = new ArrayList<>();
        for(Chat chat : client.getChats())
            chats.add(Chat.toDTO(chat));
        chats.sort(Collections.reverseOrder(Comparator.comparing(ChatDTO::getLastModified)));

        return new ResponseEntity<>(chats, HttpStatus.OK);
    }

    @PostMapping("/{clientID}/chats/{sellerID}")
    public @ResponseBody ResponseEntity<?> addChat(@PathVariable Integer clientID,
                                                   @PathVariable Integer sellerID,
                                                   @RequestBody Date creationDate) throws ControllerException{

        // get clients participating in the chat
        Client client = (Client) Utils.findById(this.clientDAO, clientID);
        Client seller = (Client) Utils.findById(this.clientDAO, sellerID);

        // push them into a list
        List<Client> participants = new ArrayList<>();
        participants.add(client);
        participants.add(seller);

        // create chat to be stored
        Chat chat = new Chat();
        chat.setClients(participants);
        chat.setLastModified(creationDate);
        this.chatDAO.save(chat);

        return new ResponseEntity<>(chat, HttpStatus.OK);
    }

    @PostMapping("/{clientID}/chats/{chatID}/messages")
    public @ResponseBody ResponseEntity<?> sendMessage(@PathVariable Integer clientID,
                                                       @PathVariable Integer chatID,
                                                       @RequestBody Message message) throws ControllerException {
        // set client as the sender of the message
        Client sender = (Client) Utils.findById(this.clientDAO, clientID);
        message.setSender(sender);
        System.out.println(sender);

        // add message to given chat and store
        Chat chat = (Chat) Utils.findById(this.chatDAO, chatID);
        message.setChat(chat);
        chat.addMessage(message);
        chat.setLastModified(message.getTimeStamp());
        this.chatDAO.save(chat);

        return new ResponseEntity<>(chat, HttpStatus.OK);
    }
}
