package com.ebid.exception;

import lombok.Data;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

@Data
class ResponseError{
    private String message;
    private String timeStamp;

    public ResponseError(String message) {
        this.message = message;
        this.timeStamp = new SimpleDateFormat("yyyy.MM.dd - HH.mm.ss").format(new Date());
    }
}

@ControllerAdvice
public class Handler {

    @ExceptionHandler
    private ResponseEntity<ResponseError> duplicateKey(DataIntegrityViolationException e) {

        // in case the username already exists, reject the user sign-up
        ResponseError error = new ResponseError("Username exists");
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    private ResponseEntity<ResponseError> thrownByController(ControllerException e){
        return new ResponseEntity<>(new ResponseError(e.getMessage()), e.getStatus());
    }

   /* @ExceptionHandler
    private ResponseEntity<ResponseError> badRequest(Exception e){
        return new ResponseEntity<>(new ResponseError(e.getMessage()), HttpStatus.BAD_REQUEST);
    }*/
}