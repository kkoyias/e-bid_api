package com.ebid.exception;

import org.springframework.http.HttpStatus;

public class ControllerException extends Exception {
    private HttpStatus status;

    public ControllerException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {return status;}
}
