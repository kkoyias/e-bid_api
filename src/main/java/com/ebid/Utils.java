package com.ebid;

import com.ebid.exception.ControllerException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

public class Utils {

    // given a repo, get a certain record using an Integer identifier
    public static Object findById(JpaRepository repo, Integer id) throws ControllerException{
        Optional<Object> object = repo.findById(id);

        if(!object.isPresent())
            throw new ControllerException("Object with id " + id + " does not exist", HttpStatus.NOT_FOUND);

        return object.get();
    }

    // update certain fields of a given object
    public static Object updateObject(Object objectA,Object objectB) throws IllegalAccessException{
        Class type = objectA.getClass();
        if(type != objectB.getClass())
            return null;

        for(Field field : type.getDeclaredFields()){
            field.setAccessible(true);

            // update all fields supplied, but not ids and passwords if they exist
            boolean fieldIsEmpty = field.get(objectB) == null ||
                    (field.getType() == float.class && field.getFloat(objectB) == 0) ||
                    (field.getType() == int.class && field.getInt(objectB) == 0) ||
                    (field.getType() == boolean.class); // disallow boolean properties to be updated

            boolean fieldIsProtected = field.getName().equals("password") || field.isAnnotationPresent(Id.class);

            if(!fieldIsEmpty && !fieldIsProtected) {
                if(field.getType() == List.class) {
                    ((List)field.get(objectA)).clear();
                    ((List) field.get(objectA)).addAll((List) field.get(objectB));
                }
                else
                    field.set(objectA, field.get(objectB));
            }
            field.setAccessible(false);
        }
        return objectA;
    }
}
