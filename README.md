# e-bid api

## Intro

This part of the project implements the server side
of the e-bid project using the
[Spring Framework](https://spring.io/) for
the Java platform. One can run it either from the
command line using [Maven](https://maven.apache.org/)
or from any IDE like [IntelliJ IDEA](https://www.jetbrains.com/idea/).

### Kick-off

Once the application starts, it creates or updates
all tables of the schema if necessary. Setting
the attribute `spring.jpa.hibernate.ddl-auto`
of application.properties to *create*
will lead to creating a new user with role=**ROLE_ADMIN**
and save that user into the database,
as it was requested in the requirements.
The name & password of this user are set to 'admin'.

### API

This application uses a lot of end-points created
by Spring Data Rest, but also overrides plenty of them.
For a detailed description of the API, check out
[API.md](./API.md)

### Sign-Up

On sign-up, the base-64 encoded password received is
decoded and then encoded using Java PasswordEncoder.
After that, the user is stored in the database.

### Duplicate entries & Constraint Violations

To deal with duplicate entries(e.g categories with the same name)
or any constraint violation(e.g same username), @UniqueConstraint
annotations are included where necessary.

### Exception Handling

To handle Exceptions throw due to bad requests,
(e.g sign-up with existing user name) a set of Exception Handling
methods where defined in a class annotated as @ControllerAdvice
which are triggered when the corresponding exception is thrown
and return with the appropriate response to the requesting service.

### Authentication & Security

Once a request is received at the corresponding login
end-point, the application decodes the base-64 encoded
password included in the header and compares it
to the actual password, if the username exists.
Once the user is authenticated a **JSON Web Token**
is sent as a response containing the Role
and the Id of the user. That way each protected resource
can be accessed by including that token in the header of the request.
That way, the user is authorized & all resources are exposed
to those authorized only.

### Customizing response

In order to alter the default view of the model
responding with a body containing as much information
as needed,
Projection interfaces & Data Transfer Objects where defined.

### Categories

In order to create a hierarchical schema of categories,
each category is associated with another, marked as a parent
category, creating a tree for each main category(orphan).

### Entities & Relationships

#### User & Client

The Client class **extends** the User class, adding some necessary properties.
If only the id, username & password is required(e.g an admin)
then creating a User instance is more appropriate.

#### Client - Item, Bid, Rating & Purchase

A client can have/offer many items,
which shall all be removed once the account
of the client is deleted(CascadeType.REMOVE).
Each item is offered by one client, but cancelling an auction
(deleting the item) shall never remove the account of the client.
The same rule applies for the all biddings that this client made too.

Purchases are an exception, these are not removed because the admin
must be able to export all items ever offered & bought.

#### Client - Chat & View

These relationships are many to many and CascadeType.REMOVE is of course
not included in the relationship.

#### Chat - Message

A chat is composed of many messages and a message belongs to a single chat.

### Recommendations

Recommendations for a particular client are based on his
ratings and views, using
[collaborative filtering](https://en.wikipedia.org/wiki/Collaborative_filtering)
creating a N x M matrix with one row
for each client and a column for each item.
The similarity of two clients is actually the distance between their corresponding
vectors in the matrix.
To get better results, we used
[pearson correlation](https://en.wikipedia.org/wiki/Pearson_correlation_coefficient)
meaning that the **average product rating** of each neighbor was also
included in the equation.
After finding the nearest neighbors, a list is returned
with the items of their preference weighted by their
**similarity degree** with the requesting client.
The sql script that implements this part of the project can be found under
[recommendations.sql](./src/main/resources/sql/nearest-neighbors/rating-based/recommendations.sql)

## Epilogue

That was it, make sure you try this api.
Feel free to comment or even contribute to this project.
Fork this repository make some changes and issue a pull request.
It will be reviewed as soon as possible and you will get a response.
Thanks for reading.
